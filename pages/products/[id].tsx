import * as React from 'react';
import { MainLayout } from '@md-modules/shared/layouts/main';
import { ProductContainer } from '@md-sw-product/index';

const ProductPage = () => {
  return (
    <MainLayout>
      <ProductContainer />
    </MainLayout>
  );
};

export default ProductPage;
