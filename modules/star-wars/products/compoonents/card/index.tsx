import * as React from 'react';
// views
import {
  CardWrapper,
  CardImgWrapper,
  CardImg,
  CardFooter,
  CardFooterTitle,
  ViewButton,
  CardFooterPrice
} from './views';
// view components
import { ProductLink } from '../product-link';
import { Product } from '@md-modules/shared/mock';

interface Props {
  product: Product;
  onAdd: (product: Product) => void;
}

const Card: React.FC<Props> = ({ product, onAdd }) => (
  <CardWrapper key={product.id}>
    <CardImgWrapper>
      <CardImg src={'/static/images/Ben-Kenobi.jpg'} alt={`${product.name}-${product.id}`} />
    </CardImgWrapper>
    <CardFooter>
      <ProductLink pId={product.id}>
        <CardFooterTitle>{product.name}</CardFooterTitle>
        <CardFooterPrice>{product.price}$</CardFooterPrice>
      </ProductLink>
      <div>
        <ViewButton onClick={() => onAdd(product)}>Add to card</ViewButton>
        <ProductLink pId={product.id}>
          <ViewButton>Details</ViewButton>
        </ProductLink>
      </div>
    </CardFooter>
  </CardWrapper>
);

export { Card };
