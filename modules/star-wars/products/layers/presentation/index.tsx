import * as React from 'react';
// view components
import { ContentLoader } from '@md-ui/loaders/content-loader';
import { Card } from '@md-sw-products/compoonents/card';
// context
import { ProductsAPIContext } from '@md-sw-products/layers/api/products';
import { ProductsBLContext } from '@md-sw-products/layers/business';
// views
import { Wrapper } from './views';
import { BasketBLContext } from '@md-sw-basket/layers/business';

const ProductsPresentation = () => {
  const { isLoading } = React.useContext(ProductsAPIContext);
  const { productsList } = React.useContext(ProductsBLContext);
  const { addProduct } = React.useContext(BasketBLContext);

  return (
    <Wrapper>
      <ContentLoader isLoading={isLoading}>
        {productsList.map((product) => (
          <Card product={product} key={product.id} onAdd={addProduct} />
        ))}
      </ContentLoader>
    </Wrapper>
  );
};

export { ProductsPresentation };
