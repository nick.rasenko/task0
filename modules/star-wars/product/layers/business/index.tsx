import * as React from 'react';
import { ProductAPIContext } from '@md-sw-product/layers/api/product';

interface ProductInfo {
  label: string;
  value: string;
}

interface Context {
  productInfo: ProductInfo[];
}

const ProductBLContext = React.createContext<Context>({
  productInfo: []
});

const ProductBLContextProvider: React.FC = ({ children }) => {
  // add business logic here
  const { product } = React.useContext(ProductAPIContext);

  const productInfo = React.useMemo<ProductInfo[]>(
    () => {
      if (!product) {
        return [];
      }

      return [
        { label: 'Gender', value: product.gender },
        { label: 'Hair Color', value: product.hair_color },
        { label: 'Eye Color', value: product.eye_color },
        { label: 'Birth Year', value: product.birth_year },
        { label: 'Price', value: product.price + '$' }
      ];
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [typeof product === 'undefined']
  );

  return (
    <ProductBLContext.Provider
      value={{
        productInfo
      }}
    >
      {children}
    </ProductBLContext.Provider>
  );
};

export { ProductBLContextProvider, ProductBLContext };
