import * as React from 'react';
// context
import { ProductAPIContext } from '@md-sw-product/layers/api/product';
import { ProductBLContext } from '@md-sw-product/layers/business';
// view components
import { ContentLoader } from '@md-ui/loaders/content-loader';
import { ProductInfo } from '@md-sw-product/components/product-info';
// views
import {
  ContentWrapper,
  ProductDetailsContainer,
  ProductImgContainer,
  ProductInfoContainer,
  ProductName,
  Wrapper
} from './views';

const ProductPresentation = () => {
  const { isLoading } = React.useContext(ProductAPIContext);
  const { productInfo } = React.useContext(ProductBLContext);

  return (
    <ContentWrapper>
      <Wrapper>
        <ContentLoader isLoading={isLoading}>
          <ProductImgContainer>
            <img src='/static/images/Ben-Kenobi.jpg' alt='Kenobi' />
          </ProductImgContainer>
          <ProductDetailsContainer>
            <ProductName>Darth Vader</ProductName>
            <ProductInfoContainer>
              {productInfo.map((i, idx) => (
                <ProductInfo key={idx} {...i} />
              ))}
            </ProductInfoContainer>
          </ProductDetailsContainer>
        </ContentLoader>
      </Wrapper>
    </ContentWrapper>
  );
};

export { ProductPresentation };
