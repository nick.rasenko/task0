import * as React from 'react';
import {
  BasketItem,
  BasketItemAmount,
  BasketItemTitle,
  BasketModalContent,
  BasketModalWrapper,
  ButtonBasket,
  ButtonDelAll
} from '@md-sw-basket/components/basket-modal/views';
import { Product } from '@md-modules/shared/mock';

interface Props {
  basket: Array<{
    product: Product;
    amount: number;
  }>;
  total: number;
  onRemoveProduct: (id: string, all?: boolean) => void;
  basketActive: boolean;
}

const BasketModal: React.FC<Props> = ({ basket, total, onRemoveProduct, basketActive }) => (
  <BasketModalWrapper active={basketActive}>
    <BasketModalContent onClick={(e) => e.stopPropagation()}>
      YOUR BASKET
      <div>
        {basket.map((orderItem) => (
          <BasketItem key={orderItem.product.id}>
            <BasketItemTitle>{orderItem.product.name}</BasketItemTitle>
            <ButtonBasket onClick={() => onRemoveProduct(orderItem.product.id)}>-</ButtonBasket>
            <ButtonDelAll onClick={() => onRemoveProduct(orderItem.product.id, !!orderItem.amount)}>X</ButtonDelAll>
            <BasketItemAmount> {orderItem.amount}</BasketItemAmount>
          </BasketItem>
        ))}
      </div>
      <div>{total}</div>
    </BasketModalContent>
  </BasketModalWrapper>
);

export { BasketModal };
