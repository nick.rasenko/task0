import styled from 'styled-components';

export const BasketModalWrapper = styled.div<{ active: boolean }>`
  position: relative;
  height: 0;
  width: 0;
  background-color: rgba(0, 0, 0, 0.4);
  top: 0;
  left: 0;
  display: flex;
  align-items: center;
  justify-content: center;
  opacity: ${(props) => (props.active ? 1 : 0)};
  pointer-events: ${(props) => (props.active ? 'all' : 'none')};
  transition: 0.5s;
`;

export const BasketModalContent = styled.div`
  position: absolute;
  right: -20px;
  top: 23px;
  background-color: ${({ theme }) => theme.colors.gray400};
  border: 1px solid ${({ theme }) => theme.colors.gray600};
  padding: 8px 12px;
  color: ${({ theme }) => theme.colors.white};
  width: 500px;
`;

export const BasketItem = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const ButtonBasket = styled.button`
  background-color: ${({ theme }) => theme.colors.gray600};
  outline: none;
  border: 1px solid ${({ theme }) => theme.colors.gray400};
  margin-left: 150px;
  position: fixed;
`;
export const ButtonDelAll = styled.button`
  background-color: ${({ theme }) => theme.colors.gray600};
  outline: none;
  border: 1px solid ${({ theme }) => theme.colors.gray400};
  margin-left: 300px;
  position: fixed;
`;

export const BasketItemTitle = styled.div``;
export const BasketItemAmount = styled.div``;
