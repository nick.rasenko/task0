import * as React from 'react';
import { useCallback, useMemo, useState } from 'react';
import { Product } from '@md-modules/shared/mock';

interface Context {
  basket: Array<{
    product: Product;
    amount: number;
  }>;
  total: number;
  addProduct: (product: Product) => void;
  removeProduct: (id: string, all?: boolean) => void;
  setBasketActive: (active: boolean) => void;
  basketActive: boolean;
}

const BasketBLContext = React.createContext<Context>({
  basket: [],
  total: 0,
  addProduct: () => {},
  removeProduct: () => {},
  setBasketActive: () => {},
  basketActive: false
});

const BasketBLContextProvider: React.FC = ({ children }) => {
  const [basket, setBasket] = useState<Context['basket']>([]);

  const addProduct = useCallback(
    (product: Product) => {
      const orderItem = basket.find((item) => item.product === product);
      if (orderItem) {
        const index = basket.indexOf(orderItem);

        setBasket(
          basket
            .slice(0, index)
            .concat({
              product,
              amount: orderItem.amount + 1
            })
            .concat(basket.slice(index + 1))
        );
      } else {
        setBasket(basket.concat({ product, amount: 1 }));
      }
    },
    [basket]
  );

  const removeProduct = useCallback(
    (id: string, all = false) => {
      const orderItem = basket.find((item) => item.product.id === id);
      if (!orderItem) return;
      const index = basket.indexOf(orderItem);
      if (orderItem.amount === 1 || all) {
        setBasket(basket.slice(0, index).concat(basket.slice(index + 1)));
      } else {
        setBasket(
          basket
            .slice(0, index)
            .concat({
              product: orderItem.product,
              amount: orderItem.amount - 1
            })
            .concat(basket.slice(index + 1))
        );
      }
    },
    [basket]
  );

  const [basketActive, setBasketActive] = useState(false);
  const total = useMemo(
    () => basket.reduce((total, orderItem) => total + orderItem.product.price * orderItem.amount, 0),
    [basket]
  );

  return (
    <BasketBLContext.Provider
      value={{
        basket: basket,
        total,
        addProduct,
        removeProduct,
        setBasketActive,
        basketActive
      }}
    >
      {children}
    </BasketBLContext.Provider>
  );
};

export { BasketBLContextProvider, BasketBLContext };
