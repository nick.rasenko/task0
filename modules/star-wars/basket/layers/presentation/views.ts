import styled from 'styled-components';

export const BasketIconButton = styled.button`
  border: none;
  background-color: unset;
  margin: 0;
  padding: 0;
  cursor: pointer;
  display: flex;
  justify-items: center;

  img {
    height: 1em;
    width: auto;
  }
`;

export const BasketAmount = styled.span`
  color: ${({ theme }) => theme.colors.white};
  font-weight: bold;
  margin-left: 3px;
  visibility: ${({ children }) => (Number(children) > 0 ? 'visible' : 'hidden')};
`;
