import * as React from 'react';
import { BasketIconButton, BasketAmount } from './views';
import { useContext } from 'react';
import { BasketBLContext } from '@md-sw-basket/layers/business';
import { BasketModal } from '@md-sw-basket/components/basket-modal';

const BasketPresentation = () => {
  const { basket, total, removeProduct, setBasketActive, basketActive } = useContext(BasketBLContext);

  return (
    <BasketIconButton
      onClick={() => {
        setBasketActive(!basketActive);
      }}
    >
      <BasketModal basket={basket} total={total} onRemoveProduct={removeProduct} basketActive={basketActive} />
      <img src='/static/icons/shopping-basket-svgrepo-com.svg' alt='shopping basket' />
      <BasketAmount>{basket.length}</BasketAmount>
    </BasketIconButton>
  );
};

export { BasketPresentation };
